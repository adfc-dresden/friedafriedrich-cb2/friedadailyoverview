<?php

function friedadailyoverview_generateDailyOverwiew() {
    $html = "";

    $new_bookings = getUserBookings(userBookingsQueryArgs(sinceDays: 1));

    if (empty($new_bookings)) {
        $html .= "Keine neuen Buchungen";
        return $html;
    }

    $usersWithNewBookings = wp_list_pluck($new_bookings, 'post_author');
    $usersWithNewBookings = array_unique($usersWithNewBookings);

    foreach ( $usersWithNewBookings as $userId ) {

        $user = get_user_by( 'id', $userId );

        $user_email = $user->get("user_email");
        $user_first_name = $user->get("first_name");
        $user_last_name = $user->get("last_name");
        $user_address = $user->get("address");
        $user_phone = $user->get("phone");

        $user_total_bookings = countUserBookings(userBookingsQueryArgs(userId: $userId));

        $user_1mon_bookings = countUserBookings(userBookingsQueryArgs(userId: $userId, sinceDays: 31));
        $user_1mon_bookings_limit = 2;

        $user_3mon_bookings = countUserBookings(userBookingsQueryArgs(userId: $userId, sinceDays: 90));
        $user_3mon_bookings_limit = 6;

        $html .=  "<h3>$userId {$user->get("user_email")}</h3>\r\n";
        $html .=  "$user_first_name $user_last_name, $user_address; Tel. $user_phone<br>\r\n";
        $html .=  "User tot bookings: $user_total_bookings</br>";

        $html .= sprintf("<span%s>User bookings (enddate-31days;future): $user_1mon_bookings%s</span><br>",
                        $user_1mon_bookings > $user_1mon_bookings_limit ? " style='color: red;'" : "",
                        $user_1mon_bookings > $user_1mon_bookings_limit ? " > $user_1mon_bookings_limit" : ""
                        );

        $html .= sprintf("<span%s>User bookings (enddate-90days;future): $user_3mon_bookings%s</span><br>",
                    $user_3mon_bookings > $user_3mon_bookings_limit ? " style='color: red;'" : "",
                    $user_3mon_bookings > $user_3mon_bookings_limit ? " > $user_3mon_bookings_limit" : ""
                    );

        $user_3mon_bookings = getUserBookings(userBookingsQueryArgs(userId: $userId, sinceDays: 90));
        foreach ($user_3mon_bookings as $booking) {
            $bookingId = $booking->ID;
            $itemId = get_post_meta($bookingId, 'item-id', true);
            $item_name = get_the_title($itemId);

            $unix_time_start = get_post_meta($bookingId, 'repetition-start', true);
            $unix_time_end = get_post_meta($bookingId, 'repetition-end', true);

            $dateStartString = formatStringFromUnixtime($unix_time_start);
            $dateEndString = formatStringFromUnixtime($unix_time_end);

            $bookingTimeString = get_the_date("Y-m-d", $bookingId);

            $html .= "$item_name $dateStartString - $dateEndString (als $bookingId am $bookingTimeString)<br>\r\n";

        }

    }

    return $html;

}

function formatStringFromUnixtime($unixtime) {
    $date_time = new DateTime("@$unixtime");
    $date_time->setTimeZone(new DateTimeZone('GMT'));
    return $date_time->format('Y-m-d');
}

function userBookingsQueryArgs($userId=null, $sinceDays=null) {

    $queryArgs = array(
        'post_type'      => 'cb_booking',
        'posts_per_page' => -1,
        'post_status'    => array('publish', 'confirmed'),
        'order'          => 'ASC',
        'orderby'        => 'meta_value',
        'meta_key'       => 'repetition-start'
    );

    $userId = intVal($userId);
    if ($userId > 0) {
        $queryArgs['author'] = $userId;
    }

    $sinceDays = intVal($sinceDays);
    if ($sinceDays > 0) {
        $queryArgs['date_query'] = array(array('after' => "$sinceDays days ago"));
    }

    return $queryArgs;
}

function countUserBookings($queryArgs) {

    $query = new WP_Query($queryArgs);
    return $query->found_posts;

}

function getUserBookings($queryArgs) {

    return get_posts($queryArgs);

}
