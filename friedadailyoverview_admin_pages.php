<?php 

function friedadailyoverview_admin_menu() {
    add_options_page(
        "Buchungsübersicht",
        "Buchungsübersicht",
        'manage_options', // capability
        'friedadailyoverview_fields', // menu slug
        'friedadailyoverview_admin_page_contents'  // function to generate settings page
    );
}
add_action( 'admin_menu', 'friedadailyoverview_admin_menu' );

function friedadailyoverview_admin_page_contents() {
    ?>
    <h1>Einstellungen für Buchungsübersicht (friedadailyoverview)</h1>
    <form method="POST" action="options.php">
    <?php
    settings_fields( 'friedadailyoverview_fields' );
    do_settings_sections( 'friedadailyoverview_fields' );
    submit_button();
    ?>
    </form>
    <?php
}

add_action( 'admin_init', 'friedadailyoverview_settings_init' );

function friedadailyoverview_settings_init() {

    add_settings_section(
        'friedadailyoverview_setting_section',
        'Einstellungen',
        'friedadailyoverview_setting_section_callback_function', // callback function to generate section intro text
        'friedadailyoverview_fields'
    );

    add_settings_field(
       'friedadailyoverview_email_to',
       'Empfänger für Buchungsübersicht (E-Mail)',
       'markup_friedadailyoverview_email_to', // callback function to generate markup for single input field
       'friedadailyoverview_fields',
       'friedadailyoverview_setting_section'
    );

    register_setting( 'friedadailyoverview_fields', 'friedadailyoverview_email_to' );

}

function friedadailyoverview_setting_section_callback_function() {
    echo <<<EOF
    <p>Enter the e-mail-address to which the booking overview is sent. It is supposed to be activated once per day by a cronjob.</p>
    EOF;
}

function markup_friedadailyoverview_email_to() {
    ?>
    <input type="text" id="friedadailyoverview_email_to" name="friedadailyoverview_email_to" value="<?php echo esc_attr(get_option( 'friedadailyoverview_email_to' )); ?>">
    <?php
}

?>