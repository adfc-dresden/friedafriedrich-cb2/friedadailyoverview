<?php
/**
 * Plugin Name: Frieda & Friedrich Daily Overview
 * Description: Send overview of new CommonsBooking2 bookings since yesterday
 * Author: Nils Larsen
 * Author URI: https://www.friedafriedrich.de
 */

require_once "friedadailyoverview_shortcode.php";
require_once "friedadailyoverview_admin_pages.php";

function friedadailyoverview_schedule_daily_event() {
    // Ensure the event is not already scheduled
    if (!wp_next_scheduled('friedadailyoverview_daily_event')) {

        $currentDateTime = new DateTime();
        $currentDateTime->setTimeZone(wp_timezone());
        $currentDateTime->setTime(18, 0, 0);
        if ($currentDateTime < new DateTime()) {
            // If it's past, add one day to the date
            $currentDateTime->modify('+1 day');
        }

        $timestamp = $currentDateTime->getTimestamp();

        // Schedule the event to run at $timestamp. We use single event because reoccurring
        // events do not work with daylight saving time (interval would always be 24 h)
        wp_schedule_single_event($timestamp, 'friedadailyoverview_daily_event');
    }
}

register_activation_hook(__FILE__, 'friedadailyoverview_activate');
function friedadailyoverview_activate() {

    friedadailyoverview_schedule_daily_event();
}

register_deactivation_hook(__FILE__, 'friedadailyoverview_deactivate');
function friedadailyoverview_deactivate() {
    wp_clear_scheduled_hook('friedadailyoverview_daily_event');
}

add_action('friedadailyoverview_daily_event', 'friedadailyoverview_sendmail');
function friedadailyoverview_sendmail() {

    friedadailyoverview_schedule_daily_event();

    $to = get_option("friedadailyoverview_email_to");
    
    if (!is_email($to)) return;
    
    $subject = "Buchungsübersicht";
    $html = friedadailyoverview_generateDailyOverwiew();

    $headers = array(
        'Content-Type: text/html; charset=UTF-8',
    );

    $commonsbooking_options_templates = get_option('commonsbooking_options_templates');

    if (is_array($commonsbooking_options_templates)) {
        $from = $commonsbooking_options_templates['emailheaders_from-email'];
        $fromname = $commonsbooking_options_templates['emailheaders_from-name'];
        if (is_email($from)) {
            $headers[] = "From: $fromname <$from>";
        }
    }

    $result = wp_mail($to, $subject, $html, $headers);

}

?>